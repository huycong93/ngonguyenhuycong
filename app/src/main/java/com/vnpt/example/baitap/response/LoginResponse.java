package com.vnpt.example.baitap.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponse  extends CommonResponse {

    @Expose
    @SerializedName("DanhSachDonVi")
    private List<DanhSachDonVi> DanhSachDonVi;
    @Expose
    @SerializedName("UserInfo")
    private UserInfo UserInfo;
    @Expose
    @SerializedName("Token")
    private String Token;


    public List<DanhSachDonVi> getDanhSachDonVi() {
        return DanhSachDonVi;
    }

    public void setDanhSachDonVi(List<DanhSachDonVi> DanhSachDonVi) {
        this.DanhSachDonVi = DanhSachDonVi;
    }

    public UserInfo getUserInfo() {
        return UserInfo;
    }

    public void setUserInfo(UserInfo UserInfo) {
        this.UserInfo = UserInfo;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }



    public static class DanhSachDonVi {
        @Expose
        @SerializedName("DuongDanAnh")
        private String DuongDanAnh;
        @Expose
        @SerializedName("DiaChi")
        private String DiaChi;
        @Expose
        @SerializedName("Ten")
        private String Ten;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getDuongDanAnh() {
            return DuongDanAnh;
        }

        public void setDuongDanAnh(String DuongDanAnh) {
            this.DuongDanAnh = DuongDanAnh;
        }

        public String getDiaChi() {
            return DiaChi;
        }

        public void setDiaChi(String DiaChi) {
            this.DiaChi = DiaChi;
        }

        public String getTen() {
            return Ten;
        }

        public void setTen(String Ten) {
            this.Ten = Ten;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class UserInfo {
        @Expose
        @SerializedName("SoDienThoai")
        private String SoDienThoai;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("HoVaTen")
        private String HoVaTen;
        @Expose
        @SerializedName("TenDangNhap")
        private String TenDangNhap;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getSoDienThoai() {
            return SoDienThoai;
        }

        public void setSoDienThoai(String SoDienThoai) {
            this.SoDienThoai = SoDienThoai;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getHoVaTen() {
            return HoVaTen;
        }

        public void setHoVaTen(String HoVaTen) {
            this.HoVaTen = HoVaTen;
        }

        public String getTenDangNhap() {
            return TenDangNhap;
        }

        public void setTenDangNhap(String TenDangNhap) {
            this.TenDangNhap = TenDangNhap;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
