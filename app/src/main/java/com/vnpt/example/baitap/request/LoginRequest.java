package com.vnpt.example.baitap.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginRequest {

    @Expose
    @SerializedName("TokenFirebase")
    private String TokenFirebase;
    @Expose
    @SerializedName("MaHeDieuHanh")
    private String MaHeDieuHanh;
    @Expose
    @SerializedName("MaMay")
    private String MaMay;
    @Expose
    @SerializedName("MatKhau")
    private String MatKhau;
    @Expose
    @SerializedName("TenDangNhap")
    private String TenDangNhap;

    public LoginRequest(String tokenFirebase, String maHeDieuHanh, String maMay, String matKhau, String tenDangNhap) {
        TokenFirebase = tokenFirebase;
        MaHeDieuHanh = maHeDieuHanh;
        MaMay = maMay;
        MatKhau = matKhau;
        TenDangNhap = tenDangNhap;
    }

    public String getTokenFirebase() {
        return TokenFirebase;
    }

    public void setTokenFirebase(String TokenFirebase) {
        this.TokenFirebase = TokenFirebase;
    }

    public String getMaHeDieuHanh() {
        return MaHeDieuHanh;
    }

    public void setMaHeDieuHanh(String MaHeDieuHanh) {
        this.MaHeDieuHanh = MaHeDieuHanh;
    }

    public String getMaMay() {
        return MaMay;
    }

    public void setMaMay(String MaMay) {
        this.MaMay = MaMay;
    }

    public String getMatKhau() {
        return MatKhau;
    }

    public void setMatKhau(String MatKhau) {
        this.MatKhau = MatKhau;
    }

    public String getTenDangNhap() {
        return TenDangNhap;
    }

    public void setTenDangNhap(String TenDangNhap) {
        this.TenDangNhap = TenDangNhap;
    }
}
