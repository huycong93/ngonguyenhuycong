package com.vnpt.example.baitap.network;

import com.vnpt.example.baitap.request.LoginRequest;
import com.vnpt.example.baitap.response.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface APIManager {
    String SERVER_URL = "http://bms.mis.vn";
    @POST("/api/MobileNhanVien/DangNhap")
    Call<LoginResponse> Login(@Body LoginRequest loginRequest);

}
