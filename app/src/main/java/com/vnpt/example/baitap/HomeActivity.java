package com.vnpt.example.baitap;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.vnpt.example.baitap.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}